//package simran;



import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
public class Transactions 
{
	public static void main(String args[]) throws FileNotFoundException, IOException, ParseException
	{
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		System.out.println("accountId:");
		String accountID = buf.readLine();
		
		System.out.println("from:");
		String from = buf.readLine();
		
		System.out.println("to:");
		String to = buf.readLine();
		Date date1=formatter.parse(from); 
		Date date2=formatter.parse(to); 
		
		int numberOfRecords = 0;
		List<List<String>> records = new ArrayList<>();
		try (Scanner scanner = new Scanner(new File("/Users/simranbhutani/Desktop/Transactions.csv"));
				) {
			scanner.useDelimiter(",");
		    while (scanner.hasNextLine()) {
		    	numberOfRecords++;
		        records.add(getRecordFromLine(scanner.nextLine()));
		        
		    }
		}
		numberOfRecords--;
		
		String transactionID[] = new String[numberOfRecords];
		String fromAccountID[] = new String[numberOfRecords];
		String toAccountID[] = new String[numberOfRecords];
		String createdAt[] = new String[numberOfRecords];
		double amount[] = new double[numberOfRecords];
		String transactionType[] = new String[numberOfRecords];
		String relatedTransaction[] = new String[numberOfRecords];
		
		int m = 0;
		List<String> k = new ArrayList<String>();
		for(int i = 1; i < records.size(); i++) 
		{
			
			  k = (records.get(i));
			  transactionID[m] = (k.get(0));
			  fromAccountID[m] = (k.get(1));
			  toAccountID[m] = (k.get(2));
			  createdAt[m] = (k.get(3));
			  amount[m] = Double.parseDouble((k.get(4)));
			  transactionType[m] = (k.get(5));
			  

			  if(transactionType[m].equalsIgnoreCase("PAYMENT"))
			  {
				  relatedTransaction[m] = "NULL";
				  //
			  }
			  else
			  {
				  relatedTransaction[m] = k.get(6);
			  }
			  
			  m++;
		
		}
	
		double relativeSum = 0;
		int numberOfTr = 0;
		for(int i=0;i<numberOfRecords;i++)
		{
			
			Date created=formatter.parse(createdAt[i]);
			if (created.after(date1) && created.before(date2))
					{
				if(fromAccountID[i].equalsIgnoreCase(accountID)) 
				{
					relativeSum = relativeSum - amount[i];
					numberOfTr++;
				}
				if(toAccountID[i].equalsIgnoreCase(accountID))
				{
					relativeSum = relativeSum + amount[i];
					numberOfTr++;
					
				}
				
				
					}
			if(transactionType[i].equalsIgnoreCase("REVERSAL"))
			{
				
				String t = relatedTransaction[i];
				
				for(int j=0;j<numberOfRecords;j++)
				{
					if(t.equalsIgnoreCase(transactionID[j]))
							{
						         if(fromAccountID[j].equals(accountID))
						         {
						        	 relativeSum = relativeSum + amount[j];
						        	 numberOfTr--;
						         }
						         if(toAccountID[j].equals(accountID))
						         {
						        	 relativeSum = relativeSum - amount[j];
						        	 numberOfTr--;
						         }
							}
				}
				
			}
			
		}
		String finalRel = "";
		if(relativeSum<0)
		{
			finalRel = finalRel + "-" + "$" + Math.abs(relativeSum);
		}
		else
		{
			finalRel = finalRel + "$" +relativeSum;
			
		}
		System.out.println("Relative balance for the period is: "+finalRel);
		System.out.println("Number of transactions included is: "+numberOfTr);

		
    }

	private static List<String> getRecordFromLine(String nextLine) {
		 List<String> values = new ArrayList<String>();
		    try (Scanner rowScanner = new Scanner(nextLine)) {
		        rowScanner.useDelimiter(",");
		        while (rowScanner.hasNext()) {
		            values.add(rowScanner.next());
		        }
		    }
		    return values;
	}
	

}
